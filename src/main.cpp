/**
* @file	    main.cpp
* @brief	Arquivo com a função principal do programa.
* @author   Pedro Emerick (p.emerick@live.com)
* @since    30/08/2018
* @date     08/09/2018
*/

#include <iostream>
using std::cout;
using std::endl;
using std::cerr;

#include <string>
using std::string;

#include "manip_arq.h"
#include "rc4.h"

/**
* @brief    Função que verifica se os argumentos passados ao programa são válidos
* @param    argc Números de argumentos passados
* @param    argv Vetor com os argumentos passados
*/
void verifica_arg (int argc, char *argv[]) 
{
    // Faz a validação dos argumentos passados de acordo com a opção desejada
    if (argc != 5 && argc != 4) {
        cerr << "--> Argumentos invalidos !" << endl;
        cerr << "Se deseja cifrar a mensagem use: ./prog cifrar chave mensagem.txt mensagem_saida.txt" << endl;
        cerr << "Se deseja decifrar a mensagem use: ./prog decifrar chave mensagem.txt" << endl;

        exit(1);
    }

    string opcao = argv[1];

    if (opcao != "cifrar" && opcao != "decifrar") {
        cerr << "--> Opção invalida !" << endl;
        cerr << "Se deseja cifrar a mensagem use: ./prog cifrar chave mensagem.txt mensagem_saida.txt" << endl;
        cerr << "Se deseja decifrar a mensagem use: ./prog decifrar chave mensagem.txt" << endl;

        exit (1);
    } else if (opcao == "cifrar" && argc != 5) {
        cerr << "--> Opção invalida !" << endl;
        cerr << "Se deseja cifrar a mensagem use: ./prog cifrar chave mensagem.txt mensagem_saida.txt" << endl;
        cerr << "Se deseja decifrar a mensagem use: ./prog decifrar chave mensagem.txt" << endl;

        exit (1);
    } else if (opcao == "decifrar" && argc != 4) {
        cerr << "--> Opção invalida !" << endl;
        cerr << "Se deseja cifrar a mensagem use: ./prog cifrar chave mensagem.txt mensagem_saida.txt" << endl;
        cerr << "Se deseja decifrar a mensagem use: ./prog decifrar chave mensagem.txt" << endl;

        exit (1);
    }
}

/**
* @brief Função principal do programa.
*/
int main (int argc, char *argv[]) 
{
    verifica_arg (argc, argv);
    string opcao = argv[1];

    string mensagem = ler_msg(argv[3]);
    string saida = rc4 (argv[2], mensagem);

    if (opcao == "cifrar") {
        cout << "--> Mensagem cifrada com sucesso, e salva no arquivo de saida !" << endl;
        gravar_msg (saida, argv[4]);
    } else {
        cout << "--> Mensagem decifrada com sucesso:" << endl << endl;
        cout << saida << endl;
    }
   
   return 0;
}