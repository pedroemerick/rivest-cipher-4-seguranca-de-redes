/**
* @file	    manip_arq.cpp
* @brief	Implementção das funções que manipulam arquivos, gravando e lendo mensagens de arquivos
* @author   Pedro Emerick (p.emerick@live.com)
* @since    27/08/2018
* @date     08/09/2018
*/

#include "manip_arq.h"

#include <fstream>
using std::ifstream;
using std::ofstream;

#include <iostream>
using std::endl;
using std::cerr;

#include <string>
using std::string;

/**
* @brief Função que recebe o nome/local do arquivo em que se encontra a mensagem
* @param nome_arq Nome do arquivo a ser lido
* @return Mensagem lida do arquivo
*/
string ler_msg (string nome_arq)
{
    ifstream arquivo (nome_arq);
    if (!arquivo)
    {
        cerr << "--> Erro ao ler mensagem !" << endl;
        exit (1);
    }

    string mensagem;
    while (!arquivo.eof()) //enquanto end of file for false continua
    {
        string str_temp;
        getline (arquivo, str_temp);
        mensagem += str_temp;
        mensagem += "\n";
    }

    mensagem.erase(mensagem.end()-1, mensagem.end());

    arquivo.close();

    return mensagem;
}

/**
* @brief Função que grava a mensagem cifrada em um arquivo
* @param mensagem Mensagem cifrada a ser gravada no arquivo
* @param nome_arq Nome do arquivo a ser gravado
*/
void gravar_msg (string mensagem, string nome_arq)
{
    ofstream arquivo (nome_arq);

    if (!arquivo)
    {
        cerr << "--> Erro ao gravar mensagem cifrada !" << endl;
        exit (1);
    }

    arquivo << mensagem;
    arquivo.close ();
}