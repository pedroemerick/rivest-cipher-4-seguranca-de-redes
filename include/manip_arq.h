/**
* @file	    manip_arq.h
* @brief	Declaracao do prototipo das funções que manipulam arquivos, 
            gravando e lendo mensagens de arquivos
* @author   Pedro Emerick (p.emerick@live.com)
* @since    27/08/2018
* @date     08/09/2018
*/

#ifndef MANIP_ARQ_H
#define MANIP_ARQ_H

#include <string>
using std::string;

/**
* @brief Função que recebe o nome/local do arquivo em que se encontra a mensagem
* @param nome_arq Nome do arquivo a ser lido
* @return Mensagem lida do arquivo
*/
string ler_msg (string nome_arq);

/**
* @brief Função que grava a mensagem cifrada em um arquivo
* @param mensagem Mensagem cifrada a ser gravada no arquivo
* @param nome_arq Nome do arquivo a ser gravado
*/
void gravar_msg (string mensagem, string nome_arq);

#endif